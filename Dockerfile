FROM python:3.9

RUN apt update && apt install -y pipx
RUN echo 'export PATH="$PATH:/root/.local/bin"' >> /root/.bashrc

RUN pipx install poetry==1.6.1

WORKDIR /tmp
RUN wget -q https://github.com/pulumi/pulumi/releases/download/v3.106.0/pulumi-v3.106.0-linux-arm64.tar.gz -O - | tar xzvf - && mv pulumi/* /usr/local/bin/
